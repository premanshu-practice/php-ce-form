<?php
$connection = mysqli_connect('localhost', 'root', '', 'db_colorelephant');
header('Content-Type: application/json');
if ($_POST['register'] == "success"){
	if (!empty($_FILES['uploaded_cv']['name'])) {
		$target_dir = $_SERVER['DOCUMENT_ROOT']."/color-elephant/php-ce-form/uploads/";
		$target_file = $target_dir . basename($_FILES["uploaded_cv"]["name"]);
		$allowedExts = array("pdf", "doc", "docx");
		$ext = explode(".", $_FILES["uploaded_cv"]["name"]);
	    $extension = end($ext);
		if (!in_array($extension, $allowedExts)) {
			echo json_encode(array('status' => 3));
			die();
		}
	    if (($_FILES["uploaded_cv"]["type"] == "application/pdf") || ($_FILES["uploaded_cv"]["type"] == "application/msword") || ($_FILES["uploaded_cv"]["type"] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") && ($_FILES["uploaded_cv"]["size"] < 2097152) && in_array($extension, $allowedExts)) {
			if (move_uploaded_file($_FILES["uploaded_cv"]["tmp_name"], $target_file)) {
			} else {
				echo json_encode(array('status' => 1));
				die();
			}
		} elseif ($file_size > 2097152) {
			echo json_encode(array('status' => 2));
			die();
		}
	} else {
		echo json_encode(array('status' => 4));
		die();
	}
	$sql = "CREATE TABLE IF NOT EXISTS users (
		id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
		name VARCHAR(30) NOT NULL,
		email VARCHAR(50) NOT NULL,
		web_address VARCHAR(50) NOT NULL,
		cover_letter VARCHAR(500) NOT NULL,
		cv VARCHAR(30) NOT NULL,
		que_working INT(1) UNSIGNED NOT NULL,
		submission_date DATE NOT NULL
	)";
	$create_query = mysqli_query($connection, $sql);
	if(!$create_query) {
		echo json_encode(array('status' => 5));
		die();
	}

	$reg_name			=	$_POST['reg_name'];
	$reg_email			=	$_POST['reg_email'];
	$reg_web_address	=	$_POST['reg_web_address'];
	$reg_cover_letter	=	$_POST['reg_cover_letter'];
	$uploaded_cv		=	basename($_FILES["uploaded_cv"]["name"]);
	$reg_wokring_que	=	$_POST['reg_wokring_que'];
	if ($reg_wokring_que == 'yes') {
		$ques_value 	= 	0;
	} else {
		$ques_value 	= 	1;
	}
	$captcha			=	$_POST['captcha'];
	$submission_date	=	date('Y-m-d H:i:s');


	if ($reg_name == '' || $reg_email == '' || $reg_web_address == '' || $reg_cover_letter == '' || $reg_wokring_que == '' ) {
		echo json_encode(array('status' => 7));
		die();	
	}
	session_start();
	if(!(isset($captcha)&&$captcha!=""&&$_SESSION["code"]==$captcha)) {
		echo json_encode(array('status' => 6));
		die();
	}
	
	$select_sql = "SELECT email FROM users WHERE email = '$reg_email'";
	$select_result = mysqli_query($connection, $select_sql);
	if (mysqli_num_rows($select_result) > 0) {
		echo json_encode(array('status' => 8));
		die();	
	}

	$q = "INSERT INTO users (name, email, web_address, cover_letter, cv, que_working, submission_date) VALUES ('$reg_name', '$reg_email', '$reg_web_address', '$reg_cover_letter', '$uploaded_cv', $ques_value, '$submission_date')";
	$query = mysqli_query($connection, $q);
	if($query) {
		echo json_encode(array('status' => 10));
		die();
	} else {
		echo json_encode(array('status' => 9));
		die();
	}
	$connection->close();
}
?>