<?php session_start(); 
	$_SESSION['user_id'] = 11;
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Practice Assignment</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Including external style.css -->
		<link rel="stylesheet" href="css/style.css">
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/register.js" ></script>
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/login.js" ></script>
		<!-- Including external edit-user.js JS -->
		<script src="js/edit-user.js"></script>
		<!-- Sweet alert JS -->
		<script src="js/sweetalert-dev.js"></script>
		<!-- Sweet alert CSS -->
		<link rel="stylesheet" href="css/sweetalert.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
		<?php 	if (!isset($_SESSION['user_id'])) {	?>
					<h1>Your Session Has Been Expired. <a href="/color-elephant/php-ce-form/login.php">Click Here</a> to Login again.</h1>
				<?php die(); ?>
				</div>
		<?php	}	?>	
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h1>All Users Information.</h1>
				</div>
				<div class="col-md-2"></div>
			</div>
		<?php
			$user_id = $_SESSION['user_id'];
			$connection = mysqli_connect('localhost', 'root', '', 'db_colorelephant');
			$select_sql = "SELECT * FROM users WHERE id = $user_id";
			$select_result = mysqli_query($connection, $select_sql);
			if (mysqli_num_rows($select_result) > 0) {
				while($row = mysqli_fetch_assoc($select_result)) { ?>
					<div class="row">
						<div class="col-md-2"><?php echo $row['name']; ?></div>
						<div class="col-md-8"></div>
						<div class="col-md-2"></div>				
					</div>			
		<?php 	}
			} else {
				die();
			}
		?>
		<?php
			$select_all_sql = "SELECT * FROM users";
			$select_all_result = mysqli_query($connection, $select_all_sql);
			if (mysqli_num_rows($select_all_result) > 0) {
				while($row_all = mysqli_fetch_assoc($select_all_result)) { ?>
					<div class="row">
						<div class="col-md-3"><?php echo $row_all['id']; ?></div>
						<div class="col-md-3"><?php echo $row_all['name']; ?></div>
						<div class="col-md-3"><?php echo $row_all['email']; ?></div>
						<div class="col-md-3"><button class="btn btn-primary editRemove" type="button" value="<?php echo $row_all['id']; ?>" onclick="edit_open_modal(event)">Edit</button></div>
					</div>			
		<?php 	}
			} else {
				die();
			}
		?>
		</div>
		<!-- All Users Details Ends Here -->
	</body>
</html>