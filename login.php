<!DOCTYPE html>
<html>
	<head>
		<title>Practice Assignment</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Including external style.css -->
		<link rel="stylesheet" href="css/style.css">
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/register.js" ></script>
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/login.js" ></script>
		<!-- Sweet alert JS -->
		<script src="js/sweetalert-dev.js"></script>
		<!-- Sweet alert CSS -->
		<link rel="stylesheet" href="css/sweetalert.css">
	</head>
	<body>
		<div class="col-md-3"></div>

		<!-- Login Starts Here -->
		<div class="col-md-6" id="login-div">
			<h1>Login Here</h1>
			<form id="login-form" action="">
				<div class="form-group">
					<label for="log-email">Email</label>
					<input type="email" name="log-email" class="form-control" id="log-email" aria-describedby="emailHelp" placeholder="Enter Email">
					<p id="log_error_valid_email" class="log_error_valid_email"> * Please Enter Valid Email.</p>
				</div>
				<div class="form-group">
					<button id="btn-login" type="button" class="btn btn-primary" name="log-submit">Login</button>
				</div>
			</form>
		</div>
		<!-- Login Ends Here -->
		<div class="col-md-3"></div>
	</body>
</html>