$(document).ready(function() {
	// Register form submit click function
	$("#btn-register").click(function(e){
		e.preventDefault();
	    var reg_name			=	$("input[name='reg-name']").val();
	    var reg_email			=	$("input[name='reg-email']").val();
	    var reg_web_address		=	$("input[name='reg-web-address']").val();
	    var reg_cover_letter	=	$("textarea[name='reg-cover-letter']").val();
	    var reg_cv       		= 	document.getElementById('reg-cv');
		var uploaded_cv   		= 	reg_cv.files[0];
	    var reg_wokring_que		=	$("input[name='reg-wokring-que']").val();
	    var captcha				=	$("input[name='captcha']").val();
	    var formData = new FormData();

	    var err = new Array();
		if (reg_name == null || reg_name == '') {
			$('#reg-name').addClass("error");
			err[0] = false;
		} else {
			$('#reg-name').removeClass("error");  
			err[0] = true;
		}
		if (reg_email == null || reg_email == '') {
			$('#reg-email').addClass("error");
			err[1] = false;
		} else {
			$('#reg-email').removeClass("error");  
			err[1] = true;
		}
		if (reg_web_address == null || reg_web_address == '') {
			$('#reg-web-address').addClass("error");
			err[2] = false;
		} else {
			$('#reg-web-address').removeClass("error");  
			err[2] = true;
		}
		if (reg_cover_letter == null || reg_cover_letter == '') {
			$('#reg-cover-letter').addClass("error");
			err[3] = false;
		} else {
			$('#reg-cover-letter').removeClass("error");  
			err[3] = true;
		}
		if (uploaded_cv == undefined || uploaded_cv == '') {
			$('#reg-cv').addClass("error");
			err[4] = false;
		} else {
			$('#reg-cv').removeClass("error");  
			err[4] = true;
		}
		if (captcha == null || captcha == '') {
			$('#captcha').addClass("error");
			err[5] = false;
		} else {
			$('#captcha').removeClass("error");  
			err[5] = true;
		}
		if($.inArray(false, err) != -1){
			sweetAlert("Oops...", "Please Fill All The Fields.", "error");
			return false;
		}
		function isValidEmailAddress(emailAddress) {
		    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		    return pattern.test(emailAddress);
		};
		if( !isValidEmailAddress( reg_email ) ) { 
			$('#reg-email').addClass("error");
			document.getElementById('error_valid_email').style.display = 'block';
			sweetAlert("Oops...", "Please Enter A Valid Email.", "error");
			return false;
		} else {
			$('#reg-email').removeClass("error");
			document.getElementById('error_valid_email').style.display = 'none';
		}
		function isUrlValid(url) {
		    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
		}
		if(isUrlValid(reg_web_address)) {
		    $('#reg-web-address').removeClass("error");
			document.getElementById('error_valid_url').style.display = 'none';
		} else {
			$('#reg-web-address').addClass("error");
			document.getElementById('error_valid_url').style.display = 'block';
			sweetAlert("Oops...", "Please Enter A Valid URL.", "error");
			return false;
		}
		if(uploaded_cv.size > 2097152){
			$('#reg-cv').addClass("error");
			document.getElementById('file_size_exceeds').style.display = 'block';
			sweetAlert("Oops...", "Uploaded File Should Be Less Than 2MB.", "error");
			return false;
		} else {
			$('#reg-cv').removeClass("error");
			document.getElementById('file_size_exceeds').style.display = 'none';	
		}
		var val = uploaded_cv.name;
		var file_type = val.substr(val.lastIndexOf('.')).toLowerCase();
		if (file_type  === '.pdf' || file_type  === '.doc' || file_type  === '.txt') {
			$('#reg-cv').removeClass("error");
			document.getElementById('error_file_type').style.display = 'none';		
		} else {
			$('#reg-cv').addClass("error");
			document.getElementById('error_file_type').style.display = 'block';
			sweetAlert("Oops...", "Please select .pdf or .doc or .txt format of file.", "error");
			return false; 
		}

	    formData.append( 'reg_name', reg_name );
	    formData.append( 'reg_email', reg_email );
	    formData.append( 'reg_web_address', reg_web_address );
	    formData.append( 'reg_cover_letter', reg_cover_letter );
	    formData.append( 'uploaded_cv', uploaded_cv );
	    formData.append( 'reg_wokring_que', reg_wokring_que );
	    formData.append( 'captcha', captcha );
	    formData.append('register', 'success');
	    
	    $.ajax({
			url: 'actions/register-process.php',
			type: 'POST',
			data: formData,
			contentType:false,
			processData:false,
			success: function(data){
				/*alert(data.status);*/
				if(data.status == 1 && data.status == 5 && data.status == 9){
					sweetAlert("Oops...", "Something went wrong!", "error");
				}
				if(data.status == 2){
					sweetAlert("Oops...", "File Size Exceeds The Upload Size!", "error");
				}
				if(data.status == 3){
					sweetAlert("Oops...", "File Should Be PDF or Document.", "error");
				}
				if(data.status == 4){
					sweetAlert("Oops...", "Please upload your CV", "error");
				}
				if(data.status == 6){
					sweetAlert("Oops...", "Please Enter Valid Captcha", "error");
				}
				if(data.status == 7){
					sweetAlert("Oops...", "Please Fill All The Fields.", "error");
				}
				if(data.status == 8){
					sweetAlert("Oops...", "Email Already Exists. Please Enter Different Email.", "error");
				}
				if(data.status == 10){
					swal({
						title: "Registered Successfully.", 
						text: "Do You Want To Login ?", 
						type: "success",
						showCancelButton: true
					}, function() {
						var url = '/color-elephant/php-ce-form/login.php';
						window.location.href = url;
					});
				}
			}
		});
	});
});