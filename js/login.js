$(document).ready(function() {
	// Register form submit click function
	$("#btn-login").click(function(e){
		alert('Wokring');
		var log_email	=	$("input[name='log-email']").val();
		if (log_email == null || log_email == '') {
			$('#log-email').addClass("error");
			return false;
		} else {
			$('#log-email').removeClass("error");  
		}
		function isValidEmailAddress(emailAddress) {
		    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		    return pattern.test(emailAddress);
		};
		if( !isValidEmailAddress( log_email ) ) { 
			$('#log-email').addClass("error");
			document.getElementById('log_error_valid_email').style.display = 'block';
			sweetAlert("Oops...", "Please Enter A Valid Email.", "error");
			return false;
		} else {
			$('#log-email').removeClass("error");
			document.getElementById('log_error_valid_email').style.display = 'none';
		}
		var formData = new FormData();
		formData.append( 'log_email', log_email );
		formData.append('login', 'success');
		alert('success');
		$.ajax({
			url: 'actions/login-process.php',
			type: 'POST',
			data: formData,
			contentType:false,
			processData:false,
			success: function(data){
				alert(data.status);
				if(data.status == 1){
					swal({
						title: "Login Successfully.", 
						text: "Click Ok", 
						type: "success",
						showCancelButton: false
					},function() {
						window.location.href = "/color-elephant/php-ce-form/all-users.php";
					});
				} else if (data.status == 0) {
					sweetAlert("Oops....", "Email Doesn't Match.", "error");
				} else {
					sweetAlert("Oops....", "Something went wrong!", "error");
				}		
			}
		});
	});
});