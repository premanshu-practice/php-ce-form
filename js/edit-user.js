function edit_open_modal(e){
	var user_id = e.currentTarget.value;
	var formData = new FormData();
	formData.append( 'user_id', user_id );
	formData.append('edit', 'success');
	$.ajax({
		url: 'actions/edit-modal.php',
		type: 'POST',
		data: formData,
		contentType:false,
		processData:false,
		success: function(data){
			alert(data.status);
			if(data.status == 1){
				swal({
					title: "Login Successfully.", 
					text: "Click Ok", 
					type: "success",
					showCancelButton: false
				},function() {
					window.location.href = "/color-elephant/php-ce-form/all-users.php";
				});
			} else if (data.status == 0) {
				sweetAlert("Oops....", "Email Doesn't Match.", "error");
			} else {
				sweetAlert("Oops....", "Something went wrong!", "error");
			}		
		}
	});
}
