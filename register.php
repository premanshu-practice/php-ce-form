<!DOCTYPE html>
<html>
	<head>
		<title>Practice Assignment</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- Including external style.css -->
		<link rel="stylesheet" href="css/style.css">
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/register.js" ></script>
		<!-- Including external registration.js -->
		<script type="text/javascript" src="js/login.js" ></script>
		<!-- Sweet alert JS -->
		<script src="js/sweetalert-dev.js"></script>
		<!-- Sweet alert CSS -->
		<link rel="stylesheet" href="css/sweetalert.css">
	</head>
	<body>
		<div class="container" id="refresh-div">
		<div id="woof_html_buffer" class="woof_info_popup" style="display: none;">Loading...</div>
			<div class="row">
				<div class="col-md-3"></div>	
				<!-- Registration starts here -->
				<div class="col-md-6" id="register-div">
					<h1>Registration Form</h1>
					<form id="registration-form" action="">
						<div class="form-group">
							<label for="reg-name">Name</label>
							<input type="text" name="reg-name" class="form-control" id="reg-name" placeholder="Enter Name">
						</div>
						<div class="form-group">
							<label for="reg-email">Email</label>
							<input type="email" name="reg-email" class="form-control" id="reg-email" aria-describedby="emailHelp" placeholder="Enter Email">
							<p id="error_valid_email" class="error_valid_email"> * Please Enter Valid Email.</p>
						</div>
						<div class="form-group">
							<label for="reg-web-address">Web Address</label>
							<input type="url" name="reg-web-address" class="form-control" id="reg-web-address" placeholder="Enter Web Address">
							<p id="error_valid_url" class="error_valid_url"> * Please Enter Valid URL.</p>
						</div>
						<div class="form-group">
							<label for="reg-cover-letter">Cover Letter</label>
							<textarea name="reg-cover-letter" class="form-control" id="reg-cover-letter" placeholder="Enter Cover Letter" ></textarea>
						</div>
						<div class="form-group">
							<label for="reg-image">File Input</label>
							<input id="reg-cv" type='file' name="reg-cv" />
							<p id="file_size_exceeds" class="file_size_exceeds"> * Uploaded File Should Be Less Than 2MB.</p>
							<p id="error_file_type" class="error_file_type"> * Please select .pdf or .doc or .txt format of file.</p>
							<small id="fileHelp" class="form-text text-muted">Upload Your CV. Format should be PDF or Doc.</small>
						</div>
						<fieldset class="form-group">
							<legend>Do you like working?</legend>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="reg-wokring-que" id="reg-working-yes" value="yes" checked> Yes
								</label>
							</div>
							<div class="form-check">
								<label class="form-check-label">
									<input type="radio" class="form-check-input" name="reg-wokring-que" id="reg-working-no" value="no"> No
								</label>
							</div>
						</fieldset>
						<div class="form-group">
							<label for="reg-captcha">Captcha</label></br>
							<img src="captcha/captcha.php" /></br></br>
							<input id="captcha" name="captcha" type="text"></br>
							<small id="fileHelp" class="form-text text-muted">Enter Image Text.</small>
						</div>
						<div class="form-group">
							<button id="btn-register" type="button" class="btn btn-primary" name="reg-submit">Register</button>
						</div>
					</form>
				</div>
				<!-- Registration Ends Here -->
				<div class="col-md-3"></div>
			</div>
		</div>
	</body>
</html>